import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './guards/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'portador', loadChildren: './portador/portador.module#PortadorModule',
    data: {
      breadcrumb: 'Início'
    }
  },
  { path: 'analise', loadChildren: './analise/analise.module#AnaliseModule',
    data: {
      breadcrumb: 'Início'
    }
  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
