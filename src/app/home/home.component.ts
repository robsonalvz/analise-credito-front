import { Component, OnInit } from '@angular/core';
import { PortadorService, AuthenticationService } from '../services';
import { Portador } from '../models/Portador';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loading = false;
  portadors: Portador[];


  constructor(private portadorService: PortadorService) { }

  ngOnInit() {
    this.loading = true;
    this.portadorService.getAll().pipe().subscribe(portadors => {
        this.loading = false;
        this.portadors = portadors['data'];
    });
  }

}
