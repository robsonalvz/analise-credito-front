import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortadorAddComponent } from './portador-add/portador-add.component';
import { PortadorListComponent } from './portador-list/portador-list.component';
import { PortadorRoutingModule } from './portador-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PortadorAddComponent, PortadorListComponent],
  imports: [
    CommonModule,
    PortadorRoutingModule,
    ReactiveFormsModule,
  ]
})
export class PortadorModule { }
