import { Component, OnInit } from '@angular/core';
import { Portador } from 'src/app/models/Portador';
import { PortadorService } from 'src/app/services';

@Component({
  selector: 'app-portador-list',
  templateUrl: './portador-list.component.html',
  styleUrls: ['./portador-list.component.css']
})
export class PortadorListComponent implements OnInit {
  loading = false;
  portadors: Portador[];
  constructor(private portadorService: PortadorService) { }

  ngOnInit() {
    this.loading = true;
    this.portadorService.getAll().pipe().subscribe(portadors => {
        this.loading = false;
        this.portadors = portadors['data'];
    });
  }

}
