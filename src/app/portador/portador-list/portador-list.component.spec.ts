import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortadorListComponent } from './portador-list.component';

describe('PortadorListComponent', () => {
  let component: PortadorListComponent;
  let fixture: ComponentFixture<PortadorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortadorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortadorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
