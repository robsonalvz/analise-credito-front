import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortadorListComponent } from './portador-list/portador-list.component';
import { PortadorAddComponent } from './portador-add/portador-add.component';


const routes: Routes = [
  { path: '', component: PortadorListComponent },
  { path: 'add', component: PortadorAddComponent, data: {breadcrumb: 'Create'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortadorRoutingModule { }
