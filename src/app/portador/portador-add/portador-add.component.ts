import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PortadorService } from 'src/app/services';
import { Portador } from 'src/app/models/Portador';

@Component({
  selector: 'app-portador-add',
  templateUrl: './portador-add.component.html',
  styleUrls: ['./portador-add.component.css']
})
export class PortadorAddComponent implements OnInit {
  public portadorForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private service: PortadorService) { }

  ngOnInit() {
    this.portadorForm = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.maxLength(255)]],
      email: [null, [Validators.email]],
      cpf: [null , [Validators.required]],
      idade: [null, [Validators.required]],
      sexo: [null, [Validators.required]],
      renda: [null, [Validators.required]],
      estado:[null, [Validators.required]],
      estadoCivil: [null, [Validators.required]],
    });
  }
  public hasError(controlName: string, errorName: string) {
    return this.portadorForm.controls[controlName].hasError(errorName);
  }
  public onSubmit(data: any) {
    console.log(data);
    const portador = new Portador(
      data.nome,
      data.email,
      data.cpf,
      data.idade,
      data.sexo,
      data.renda,
      data.estado,
      data.estadoCivil,
    );
    this.service.save(portador).subscribe(() => {
        this.gotoList();
      }, error => {
        console.error(error);
      }
    );

  }
  public onCancel() {
    // this.submitted = false;
    this.portadorForm.reset();
  }
  gotoList() {
    this.router.navigate(['/portador']).then(() => {
      console.log('Redirecting to list view');
    });
  }

}
