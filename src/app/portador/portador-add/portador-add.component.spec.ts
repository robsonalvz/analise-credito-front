import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortadorAddComponent } from './portador-add.component';

describe('PortadorAddComponent', () => {
  let component: PortadorAddComponent;
  let fixture: ComponentFixture<PortadorAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortadorAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortadorAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
