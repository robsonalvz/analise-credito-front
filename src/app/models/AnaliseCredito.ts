import { Portador } from './Portador';

export class AnaliseCredito {
  id: number;
  portador: Portador;
  status: string;
}
