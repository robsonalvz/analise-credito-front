export class Portador {
  constructor(nome:string, email:string, cpf:string, idade:string, sexo:string, renda:string, estado:string, estadoCivil:string ){
      this.nome=nome;
      this.sexo=sexo;
      this.idade=idade;
      this.email=email;
      this.cpf=cpf;
      this.renda=renda;
      this.estado=estado;
      this.estadoCivil=estadoCivil;
  }
  id: number;
  nome: string;
  sexo: string;
  idade: string;
  email: string;
  cpf: string;
  renda: string;
  estado: string;
  estadoCivil: string;
}
