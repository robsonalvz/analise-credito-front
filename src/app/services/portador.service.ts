import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Portador } from '../models/Portador';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PortadorService {
    constructor(private http: HttpClient) { }

    save(obj: Portador): Observable<Portador> {
      return this.http.post<Portador>(`${environment.apiUrl}/api/portador`, obj);

    }
    getAll() {
        return this.http.get<Portador[]>(`${environment.apiUrl}/api/portador`);
    }
}
