import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { AnaliseCredito } from '../models/AnaliseCredito';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AnaliseCreditoService {
    constructor(private http: HttpClient) { }

    save(obj: AnaliseCredito, token:string): Observable<AnaliseCredito> {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ token
      })
      return this.http.put<AnaliseCredito>(`${environment.apiUrl}/api/analise-credito`, obj, {headers: headers});

    }
    getAll() {
        return this.http.get<AnaliseCredito[]>(`${environment.apiUrl}/api/analise-credito`);
    }
    get(id: number): Observable<AnaliseCredito> {
      const url  = `${environment.apiUrl}/api/analise-credito` + '/' + id;
      return this.http.get<any>(url).pipe(map(res => res.data));
    }
}
