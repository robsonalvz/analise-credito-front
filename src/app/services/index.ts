import { PortadorService } from './portador.service';
import { AuthenticationService } from './authentication.service';

export {PortadorService, AuthenticationService};
