import { Component, OnInit } from '@angular/core';
import { AnaliseCredito } from 'src/app/models/AnaliseCredito';
import { AnaliseCreditoService } from 'src/app/services/analise-credito.service';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-analise-list',
  templateUrl: './analise-list.component.html',
  styleUrls: ['./analise-list.component.css']
})
export class AnaliseListComponent implements OnInit {

  loading = false;
  analises: AnaliseCredito[];
  analise: AnaliseCredito;
  _listFilter = '';
  filteredAnalises: AnaliseCredito[] = [];
  user: User;
  constructor(private analiseCreditoService: AnaliseCreditoService, private router: Router) {
    this.filteredAnalises = this.analises;
    this.listFilter = '';
  }
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
      this._listFilter = value;
      this.filteredAnalises = this.listFilter ? this.doFilter(this.listFilter) : this.analises;
  }
  doFilter(filterBy: string): AnaliseCredito[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.analises.filter((analise: AnaliseCredito) =>
        analise.portador.nome.toLocaleLowerCase().indexOf(filterBy) !== -1);
}
  ngOnInit() {
    this.loading = true;
    this.analiseCreditoService.getAll().pipe().subscribe(analise => {
        this.loading = false;
        this.analises = analise['data'];
        this.filteredAnalises = this.analises;
    });
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }
  save(i: number, status: string) {
    this.analiseCreditoService.get(i).pipe(delay(500)).subscribe(analise => {
        this.analise = analise;
        this.analise.status = status;

        this.analiseCreditoService.save(this.analise, this.user.token).subscribe(() => {
          window.location.reload();
          }, error => {
            console.error(error);
          }
        );
    });

  }

}
