import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnaliseListComponent } from './analise-list.component';

describe('AnaliseListComponent', () => {
  let component: AnaliseListComponent;
  let fixture: ComponentFixture<AnaliseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnaliseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnaliseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
