import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnaliseListComponent } from './analise-list/analise-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AnaliseRoutingModule } from './analise-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AnaliseListComponent],
  imports: [
    CommonModule,
    AnaliseRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AnaliseModule { }
